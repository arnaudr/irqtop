/*
 * Copyright (C) 2013-2017 Elboulangero <elboulangero@gmail.com>
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _IRQTOP_LOG_H_
#define _IRQTOP_LOG_H_

#ifdef DEBUG
#define DEBUG_LEVEL 1
#else
#define DEBUG_LEVEL 0
#endif

#define debug(fmt, ...) if (DEBUG_LEVEL) { fprintf(stderr, __FILE__ ": " fmt "\n", ##__VA_ARGS__); }
#define warning(fmt, ...) if (1) { fprintf(stderr, __FILE__ ": warning: " fmt "\n", ##__VA_ARGS__); }
#define error(fmt, ...) if (1) { fprintf(stderr, __FILE__ ": error: " fmt "\n", ##__VA_ARGS__); }

#endif				/* _IRQTOP_LOG_H_ */
